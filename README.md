# UR Header only RTDE interface
This is a header only C++ interface to Universal Robots' Real-Time Data Exchange
interface. The purpose of the library is to mimic the RTDE python library
provided by UR, but still be just a drop in header. 

Author: Mathias Hauan Arbo
Date: 2022-08-02

# Requirements
 - Unix sockets
 - C++17 compatible compiler

# Installation
If you have [rapidxml](http://rapidxml.sourceforge.net/) already installed on
your system, copy the header `sm_rtde.hpp` to your include folder and include it
where you need it.

If you do not have rapidxml installed, copy the `rapidxml.hpp` and
`rapidxm_utils.hpp` as well.

# Examples
See the examples folder. The file `introduction.cpp` gives an introduction to
the `RTDE` class defined in the header and basic usage.

Running an example:

0. Make sure the robot can move freely if using a real robot
1. Transfer the `.urp` files from the `/examples/URP` the UR controller.
2. Edit the `robot_host` to the IP of the robot in the example you want to run.
3. `cd examples`
4. `make`
5. Run an example with `./#NAME` where `#NAME` is the one you want to try.
6. Start the corresponding program on the UR controller.



# Doxygen documentation
To get the [doxygen](https://doxygen.nl/) documentation, run `make docs` in the
examples folder. Then open `examples/doc/html/index.html`.