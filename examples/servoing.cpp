// Copyright 2022 SINTEF Manufacturing
// Author: Mathias Hauan Arbo

// This example shows moving the robot using the servoj command.
// We just use a sinusoidal motion on two joints
#include "../sm_rtde.hpp"
#include <iostream>
#include <thread>
#include <chrono>

double deg2rad(double angle) {
  return 3.1415926536*angle/180.0;
}

void print_array(std::array<double,6> arr) {
  for (std::size_t i = 0; i < 5; ++i) {
    std::cout << arr[i] << ",";
  }
  std::cout << arr[5];
}

int main() {
  // General setup
  std::string robot_host = "localhost";
  int robot_port = 30004;
  double output_frequency = 125;
  std::filesystem::path recipes_xml = 
    std::filesystem::current_path() / "recipes" / "servo.xml";
  std::array<double,6> initial_joint_position = {
    deg2rad(-90), deg2rad(-90), deg2rad(-120),
    deg2rad(-45), deg2rad(90), 0};
  const double wrist_2_amplitude = deg2rad(10);
  const double wrist_3_amplitude = deg2rad(10);
  double total_time = 3; // seconds

  // Derived values
  double delta_time = 1/output_frequency;
  std::size_t num_iterations = std::round(total_time/delta_time);

  // Establish connection
  sm_rtde::RTDE con(robot_host, robot_port, true, 1.0, output_frequency);
  if (!con.connectRTDE()) {
    std::cout << "Could not connect to RTDE." << std::endl;
  }

  // Try to make it accept protocol v2, then v1, if neither, fail.
  if (!con.request_protocol_version(2) &&
      !con.request_protocol_version(1)) {
    std::cout << "No known protocol version accepted";
    return 0;
  }

  // Then let's use a lambda to simplify writing the setpoint
  auto write_joint_setpoint = [&con](const std::array<double,6>& jset) {
    for (std::size_t i = 0; i < 6; ++i) {
      con.set_value(
        "servo_setpoint",
        "input_double_register_" + std::to_string(i),
        jset[i]);}};
  
  // Set up recipes
  bool accepted = con.parse_and_setup_recipes(recipes_xml, {"state"}, {"servo_setpoint", "servoing_state"});
    if (!accepted) {
    std::cout << "UR Controller did not accept recipes." << std::endl;
    return 0;
  }

  
  // Prepare move to initial position
  write_joint_setpoint(initial_joint_position);
  
  // Prepare starting the servoing
  con.set_value("servoing_state", "input_int_register_0", int32_t(1));

  std::cout << "Starting data synchronization" << std::endl;
  // Start data synchronization
  accepted = con.control_package_start();
  if (!accepted) {
    std::cout << "UR Controlled does not want to start data synchronization." << std::endl;
    return 0;
  }
  // Wait for the robot to have reached the desired position
  // it will notify us on output_int_register_0
  con.send_data_package("servo_setpoint");
  con.receive_data_package();
  while (con.get_value<int32_t>("state", "output_int_register_0") == 0) {
    con.receive_data_package();
  }
  con.send_data_package("servoing_state");

  // Set up control loop
  for (std::size_t i = 0; i < num_iterations; ++i) {
    con.receive_data_package();
    con.set_value(
      "servo_setpoint", 
      "input_double_register_4",
      initial_joint_position[4] + wrist_2_amplitude*std::sin(delta_time*i));
    con.set_value(
      "servo_setpoint",
      "input_double_register_5",
      initial_joint_position[5] + wrist_3_amplitude*std::sin(delta_time*i));
    con.send_data_package("servo_setpoint");
  }
  // Stop servoing
  std::cout << "Stopping data synchronization" << std::endl;
  con.set_value("servoing_state", "input_int_register_0", int32_t(0));
  con.send_data_package("servoing_state");
  // Sleep for 5 cycles to make sure it works
  std::this_thread::sleep_for(std::chrono::milliseconds(static_cast<int>(delta_time*1000*5)));
  // Shut down
  con.control_package_pause();
  con.disconnectRTDE();
  std::cout << con.get_total_skipped_package_count() << std::endl;
}