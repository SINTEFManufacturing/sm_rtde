// Copyright 2022 SINTEF Manufacturing
// Author: Mathias Hauan Arbo

// This example shows moving back and forth between two different joint setpoints.

#include "../sm_rtde.hpp"
#include <iostream>

double deg2rad(double angle) {
  return 3.1415926536*angle/180.0;
}

void print_array(std::array<double,6> arr) {
  for (std::size_t i = 0; i < 5; ++i) {
    std::cout << arr[i] << ", ";
  }
  std::cout << arr[5];
}

int main() {
  // General setup
  std::string robot_host = "localhost";
  int robot_port = 30004;
  bool print_state = true; // Print the state during data synchronization
  std::filesystem::path recipes_xml =
    std::filesystem::current_path() / "recipes" / "joint_setpoint.xml";

  // Setpoints to move
  std::array<double, 6> joint_setpoint1 = {
    deg2rad(-90), deg2rad(-90), deg2rad(-120), 
    deg2rad(-45), deg2rad(90), 0};
  std::array<double, 6> joint_setpoint2 = {
    deg2rad(-90), deg2rad(-90), deg2rad(-120),
    0, deg2rad(90), 0};
  
  // Establish connection
  sm_rtde::RTDE con(robot_host, robot_port, true);
  if (!con.connectRTDE()) {
    std::cout << "Could not connect to RTDE." << std::endl;
    return 0;
  }

  // Try to make it accept protocol v2, then v1, if neither, fail.
  if (!con.request_protocol_version(2) &&
      !con.request_protocol_version(1)) {
    std::cout << "No known protocol version accepted";
    return 0;
  }

  // Then let's use a lambda function to simplify writing the setpoint
  auto write_joint_setpoint = [&con](const std::array<double,6>& jset) {
    for (std::size_t i = 0; i < 6; ++i) {
      con.set_value(
        "joint_setpoint",
        "input_double_register_" + std::to_string(i),
        jset[i]);}};

  // Set up recipes
  bool accepted = con.parse_and_setup_recipes(recipes_xml, {"state"}, {"watchdog", "joint_setpoint"});
  if (!accepted) {
    std::cout << "UR Controller did not accept recipes." << std::endl;
    return 0;
  }

  // Always send 1 on the watchdog recipe.
  con.set_value("watchdog", "input_int_register_0", int32_t(1));

  accepted = con.control_package_start();
  if (!accepted) {
    std::cout << "UR Controller does not want to start data synchronization." << std::endl;
    return 0;
  }
  
  // Setup control loop
  int32_t value = 1;
  bool flip = true;
  std::array<double,6> actual_q;
  std::array<double,6> actual_dq;
  // Start the loop
  while (con.is_connected()) {
    // Receive current state
    con.receive_data_package();

    // Print state
    if (print_state) {
      actual_q = con.get_value<std::array<double,6>>("state", "actual_q");
      actual_dq = con.get_value<std::array<double,6>>("state", "actual_dq");
      std::cout << "actual_q: ";
      print_array(actual_q);
      std::cout << "\nactual_dq: ";
      print_array(actual_dq);
      std::cout << "\n";
    }

    // The output_int_register_0 shows whether we are ready for the next MoveJ command
    value = con.get_value<int32_t>("state", "output_int_register_0");
    if (value != 0) {
      if (flip) {
        write_joint_setpoint(joint_setpoint1);
        flip = false;
      } else {
        write_joint_setpoint(joint_setpoint2);
        flip = true;
      }
      con.send_data_package("joint_setpoint");
    }
    
    // Notify watchdog that we're still working
    con.send_data_package("watchdog");
  }
}