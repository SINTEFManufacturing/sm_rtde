// Copyright 2022 SINTEF Manufacturing
// Author: Mathias Hauan Arbo

// This example shows some of the general functions available through the RTDE
// interface.

#include "../sm_rtde.hpp"
#include <iostream>

int main() {
  // General setup
  std::string robot_host = "localhost";
  int robot_port = 30004;
  std::filesystem::path recipes_xml = 
    std::filesystem::current_path() / "recipes" / "cartesian_setpoint.xml";

  ////////////////////////////////////////////////////////////////////////
  // Class constructor arguments are:
  // - Host IP (no default), UR controller IP
  // - Port (default: 30004), UR controller port for RTDE
  // - verbose (default: false), gives more error/warning messages
  // - timeout (default: 1.0 s), time before failure when trying to send or receive on the socket.
  // - output_frequency (default: 125.0 Hz), only available with protocol version 2
  // - buffer_size (default 4096 bytes), probably no need to change this.
  // For this test, we've set the verbosity flag to true.
  sm_rtde::RTDE con(robot_host, robot_port, true);
  
  ////////////////////////////////////////////////////////////////////////
  // Connect to the robot controller, returns true if a connection could be established, false otherwise.
  if (!con.connectRTDE()) {
    std::cout << "Could not connect to RTDE." << std::endl;
    return 0;
  }

  ////////////////////////////////////////////////////////////////////////
  // Get URControl version. Returns an array of uin32_t: {Major, Minor, Bugfix, Build}.
  std::array<uint32_t,4> version = con.get_urcontrol_version();
  std::cout << "UR Controller version: ";
  for (uint32_t num : version) {
    std::cout << num << ".";
  }
  std::cout << std::endl;
  
  // Please verify that the number is correct. If an error happened, the result will say 0.0.0.0.

  ////////////////////////////////////////////////////////////////////////
  // Request that the robot controller and our RTDE client use the same protocol version.
  // Arguments are: protocol_version, default value 2, as that's likely what you have.
  bool accepted = con.request_protocol_version();
  std::cout << "UR Controller accepts protocol version: " << std::boolalpha << accepted << std::endl;

  ////////////////////////////////////////////////////////////////////////
  // Try to send a text message to the UR controller
  // Text message takes 3 arguments:
  // - Message to display as a string
  // - Text describing the source of the message, default is "C++ Client"
  // - The level of the message, default RTDE::MessageLevel::INFO_MESSAGE.
  // It returns a bool indicating if an error occurred during send.
  accepted = con.send_text_message(
    "Hello world", "C++ Client",
    sm_rtde::RTDE::MessageLevel::WARNING_MESSAGE);
  if (!accepted) {
    std::cout << "Error during send text message." << std::endl;
    return 0;
  }

  ////////////////////////////////////////////////////////////////////////
  // Parse recipes from an XML configuration file.
  //
  // An RTDE XML configuration file can contain multiple recipes that can be
  // either input to the controller, or output from the controller.
  // 
  // Parse and setup recipes
  accepted = con.parse_and_setup_recipes(recipes_xml, {"state"}, {"setp", "watchdog"});
  std::cout << "UR Controller accepted recipes: " << std::boolalpha << accepted << std::endl;
  if (!accepted) {
    std::cerr << "Some recipes were not accepted" << std::endl;
    return 0;
  }
  // It is also possible to parse and set up recipes manually.
  // Then one has to gather the recipes using
  //  sm_rtde::parse_recipes
  // and set the recipes using
  //  con.control_package_setup_outputs
  //  con.control_package_setup_inputs
  
  // RTDE has a specific set of data types it can send and receive, and this is
  // reflected in the recipe. In the sm_rtde header, this is defined with the
  // std::variant RTDEType. The mapping between C++ datatype and UR type is as
  // follows:

  // ------------------------------------------
  // | C++ type               | UR type       |
  // ------------------------------------------
  // | bool                   | BOOL          |
  // | uint8_t                | UINT8         |
  // | uint32_t               | UINT32        |
  // | uint64_t               | UINT64        |
  // | int32_t                | INT32         |
  // | double                 | DOUBLE        |
  // | std::array<double,3>   | VECTOR3D      |
  // | std::array<double,6>   | VECTOR6D      |
  // | std::array<int32_t,6>  | VECTOR6INT32  |
  // | std::array<uint32_t,6> | VECTOR6UINT32 |
  // ------------------------------------------

  // Additionally, RTDEType has std::monostate to indicate that a value is
  // uninitialized. 

  // Show some information
  std::vector<std::string> recipes;
  recipes = con.get_input_recipe_keys();
  std::cout << "Found recipes for input to the UR Controller:" << std::endl;
  for (std::string recipe_key : recipes) {
    std::cout << "\tkey: " + recipe_key << std::endl;
    for (std::string field_name : con.get_recipe_fields(recipe_key)) {
      std::cout << "\t\tfield_name: " << field_name << ", type: " << con.get_recipe_field_type(recipe_key, field_name) << std::endl;
    }
  }
  recipes = con.get_output_recipe_keys();
  std::cout << "Found recipes for output from the UR Controller:" << std::endl;
  for (std::string recipe_key : recipes) {
    std::cout << "\tkey: " + recipe_key << std::endl;
    for (std::string field_name : con.get_recipe_fields(recipe_key)) {
      std::cout << "\t\tfield_name: " << field_name << ", type: " << con.get_recipe_field_type(recipe_key, field_name) << std::endl;
    }
  }

  // Setting the value in a recipe can be done with:
  con.set_value("watchdog", "input_int_register_0", int32_t(1));
  // Note that the datatype must be correct. It does not know how to do automatic conversion when it can be ambiguous.
  
  // Retrieving the value in a recipe can be done with:
  int32_t watchdog_val = con.get_value<int32_t>("watchdog", "input_int_register_0");
  std::cout << "Found the value: " << watchdog_val << " in watchdog/input_int_register_0." << std::endl;
  
  ////////////////////////////////////////////////////////////////////////
  // Data synchronization
  std::array<double,6> current_state;
  // If we start the data synchronization, we tell the UR Controller to send us
  // packages repeatedly. Let's do so for a little bit
  con.control_package_start();
  for (std::size_t i = 0; i < 5; ++i) {

    // While we are started, we tell the RTDE class to listen for a data package
    // and fill the appropriate recipe.
    con.receive_data_package();
    current_state = con.get_value<std::array<double,6>>("state", "actual_q");
    std::cout << "State: {" 
              << current_state[0] << ", "
              << current_state[1] << ", "
              << current_state[2] << ", "
              << current_state[3] << ", "
              << current_state[4] << ", "
              << current_state[5] << "}\n";
    
    // To send a recipe is very similar, but we have to specify what we are sending
    con.send_data_package("watchdog");
  }
  // Then we can tell RTDE to stop sending us data packages and stop expecting
  // data packages
  con.control_package_pause();

  ////////////////////////////////////////////////////////////////////////
  // Other

  // The RTDE class will skip packages that come with an unexpected header. The
  // default number of allowed skipped packages is 8, and we can see how many
  // packages were skipped using:
  std::cout << "\nTotal skipped packages: " << con.get_total_skipped_package_count() << std::endl;

  // We can also check whether we still are connected as the UR controller may
  // decide to close the socket
  std::cout << "RTDE is still connected: " << std::boolalpha << con.is_connected() << std::endl;

  // We have some other functions that are pretty self-explanatory
  con.set_max_skipped_packages(10);
  con.set_timeout(0.1); //Seconds
  con.set_verbose(false);

  // And then we can close the socket when we're done. This is also done by the
  // destructor of the RTDE class.
  con.disconnectRTDE();
}