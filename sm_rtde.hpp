// Copyright 2022 SINTEF Manufacturing
// Author: Mathias Hauan Arbo
#ifndef SM_RTDE_HPP
#define SM_RTDE_HPP
#ifndef __STDC_IEC_559__
#error "Requires IEEE 754 floating point!"
#endif
#include <string>
#include <cstring>
#include <iostream>
#include <cmath>
#include <vector>
#include <limits>
#include <cstddef>
#include <algorithm>
#include <filesystem>
#include <variant>
#include <map>
#include <type_traits>

// Network related
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <sys/time.h>
#include <netdb.h>
#include <unistd.h>

// XML parsing related
#include "rapidxml.hpp"
#include "rapidxml_utils.hpp"

namespace sm_rtde{

// RTDE Datatype definitions
using RTDEType = std::variant<std::monostate, bool, uint8_t, uint32_t, uint64_t, int32_t, double, std::array<double,3>, std::array<double,6>, std::array<int32_t,6>, std::array<uint32_t,6>>;
enum Command {
  RTDE_REQUEST_PROTOCOL_VERSION = 86,  //<- ascii V
  RTDE_GET_URCONTROL_VERSION = 118,  //<- ascii v
  RTDE_TEXT_MESSAGE = 77,  //<- ascii M
  RTDE_DATA_PACKAGE = 85,  //<- ascii U
  RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS = 79,  //<- ascii O
  RTDE_CONTROL_PACKAGE_SETUP_INPUTS = 73,  //<- ascii I
  RTDE_CONTROL_PACKAGE_START = 83,  //<- ascii S
  RTDE_CONTROL_PACKAGE_PAUSE = 80,  //<- ascii P
  RECV_ANY = 0 //<- Receive anything to buffer, ascii A
};

namespace utils {

template<class T, class TypeList>
struct IsContainedIn;

template<class T, class... Ts>
struct IsContainedIn<T, std::variant<Ts...>>
  : std::bool_constant<(... || std::is_same<T, Ts>{})>
{};

/**
 * @brief Trim whitespace (space, tab, newline) from front and back of a string
 * @param str string with whitespace
 * @return string without whitespace, possibly empty
 */
std::string trim_whitespace(const std::string& str) {
  const std::string::size_type first = str.find_first_not_of(" \t\n");
  if (first == std::string::npos) { return ""; }
  const std::string::size_type last = str.find_last_not_of(" \t\n");
  return str.substr(first, last - first + 1);
}
} //  namespace: utils

namespace networking {
// Exceptions
/**
 * @brief Socket timeout exception
 * @author Mathias Hauan Arbo
 * @since Tue Jul 26 2022
 */
class timeout_exception : public std::runtime_error {
public:
  using std::runtime_error::runtime_error;
};

/**
 * @brief Attempting to pack a value that has not been initialized
 * @author Mathias Hauan Arbo
 * @since Tue Jul 26 2022
 */
class packing_exception : public std::runtime_error {
public:
  using std::runtime_error::runtime_error;
};

// Conversions
/**
 * @brief Convert seconds (double) to timeval
 * @param dval seconds as a double
 * @return timeval
 */
timeval sec2timeval(double dval) {
  long long microseconds = round(dval * 1e6);
  return {microseconds/1000000, microseconds % 1000000};
}

// Packing and unpacking TCP packets
/**
 * @brief A class that handles the outgoing information
 * @author Mathias Hauan Arbo
 * @since Tue Jul 26 2022
 */
class Packer {
public:
  Packer() : buffer_(3, std::byte(0)), timeout_(1.0) { }

  /**
   * @brief Set allowed time to wait for a send command
   * @param seconds
   */
  void set_timeout(double seconds) { timeout_ = seconds; }

  /**
   * @brief Send the buffer
   * @param socketfd socket file descriptor
   * @param clear_after clear buffer after successful send (default: true)
   * @return 0 if success, -1 error, -2 if timeout occurred
   */
  int send_buffer(int socketfd, bool clear_after = true) {
    send_all_(socketfd);
    if (clear_after) { clear(); }
    return 0;
  }
  /**
   * @brief Retrieve the packed buffer
   * @param clear_after clear buffer after copying (default: false)
   * @return packed buffer with header and payload
   */
  std::vector<std::byte> get_buffer(bool clear_after = false) {
    if (clear_after) {clear();}
    return buffer_;
  }
  /**
   * @brief Get a copy of the header
   * @param clear_after clear buffer after copying header (default: false)
   * @return vector of bytes representing the header
   */
  std::vector<std::byte> get_header(bool clear_after = false) {
    std::vector<std::byte> header;
    std::copy(buffer_.begin(), buffer_.begin() + 3, std::back_inserter(header));
    if (clear_after) {clear();}
    return header;
  }
  /**
   * @brief Get a copy of the payload
   * @param clear_after clear buffer after copying payload (default: false)
   * @return vector of bytes representing the payload
   */
  std::vector<std::byte> get_payload(bool clear_after = false) {
    std::vector<std::byte> payload;
    std::copy(buffer_.begin() + 3, buffer_.end(), std::back_inserter(payload));
    if (clear_after) {clear();}
    return payload;
  }
  /**
   * @brief Set buffer directly
   * @param buffer
   */
  void set_buffer(std::vector<std::byte> buffer) {
    buffer_.resize(buffer.size());
    std::copy(buffer.begin(), buffer.end(), buffer.begin());
  }
  /**
   * @brief Clear the buffer by assigning an empty header.
   */
  void clear() {
    buffer_.assign(3, std::byte(0));
  }

  /**
   * @brief Pack a header into the buffer
   * @param command which command to execute
   * @param size optional custom number of bytes to use of the buffer
   */
  void pack_header(uint8_t command, uint16_t size = 0) {
    if (size == 0) {
      size = buffer_.size();
    }
    buffer_[0] = static_cast<std::byte>(size >> 8);
    buffer_[1] = static_cast<std::byte>(size);
    buffer_[2] = static_cast<std::byte>(command);
  }
  /**
   * @brief Pack std::monostate
   * @param sm monostate
   * @throws networking::packing_exception as monostates are indicative of uninitialized fields.
   */
  void operator()(std::monostate& sm) {
    throw packing_exception("Trying to pack uninitialized variables.");
  }
  /**
   * @brief Pack a bool
   * @param b bool to pack
   */
  void operator()(bool b) { buffer_.push_back(static_cast<std::byte>(b ? 1 : 0)); }
  /**
   * @brief Pack a uint8_t
   * @param u8
   */
  void operator()(uint8_t u8) { buffer_.push_back(static_cast<std::byte>(u8)); }
  /**
   * @brief Pack a uint16_t
   * @param u16
   */
  void operator()(uint16_t u16) { pack_ints_<uint16_t>(u16); }
  /**
   * @brief Pack a uint32_t 
   * @param u32
   * @return (void)
   */
  void operator()(uint32_t u32) { pack_ints_<uint32_t>(u32); }
  /**
   * @brief Pack a uint64_t
   * @param u64
   */
  void operator()(uint64_t u64) { pack_ints_<uint64_t>(u64); }
  /**
   * @brief Pack a int32_t
   * @param i32
   */
  void operator()(int32_t i32) { pack_ints_<int32_t>(i32); }
  /**
   * @brief Pack a double
   * 
   * Assumes IEEE 754 encoding of the double is used on the system
   * 
   * @param d
   */
  void operator()(double d) {
    union punner { double dval; std::byte bval[8]; } res{};
    res.dval = d;
    for (std::size_t i = 8; i > 0; --i) {
      buffer_.push_back(res.bval[i-1]);
    }
  }
  /**
   * @brief Pack a 3 array of double
   * @param vd3
   */
  void operator()(std::array<double,3>& vd3) {
    pack_vectors_<std::array<double,3>::const_iterator>(vd3.begin(), vd3.end());
  }
  /**
   * @brief Pack a 6 array of double
   * @param vd6
   */
  void operator()(std::array<double,6>& vd6) {
    pack_vectors_<std::array<double,6>::const_iterator>(vd6.begin(), vd6.end());
  }
  /**
   * @brief Pack a 6 array of int32_t
   * @param vi32
   * @return (void)
   */
  void operator()(std::array<int32_t,6>& vi32) {
    pack_vectors_<std::array<int32_t,6>::const_iterator>(vi32.begin(),vi32.end());
  }
  /**
   * @brief Pack a 6 array of uint32_t
   * @param vu32
   */
  void operator()(std::array<uint32_t,6>& vu32) {
    pack_vectors_<std::array<uint32_t,6>::const_iterator>(vu32.begin(), vu32.end());
  }
  /**
   * @brief Pack a string
   * @param str
   */
  void operator()(std::string& str) {
    std::transform(
      str.begin(), str.end(), std::back_inserter(buffer_),
      [](char c){ return std::byte(c);});
  }
private:
  /**
   * @brief Send all bytes in the buffer over a socket
   * @param socketfd
   * @return -2 if timeout, -1 if other error, 0 if success
   */
  int send_all_(int socketfd) {
    fd_set file_desc_set;
    FD_ZERO(&file_desc_set);
    FD_SET(socketfd, &file_desc_set);
    std::size_t sent_bytes = 0, num_bytes = buffer_.size();
    int res = 0;
    timeval timeout = sec2timeval(timeout_);
    while (sent_bytes < num_bytes) {
      res = select(socketfd + 1, NULL, &file_desc_set, NULL, &timeout);
      if (res == 0) { return -2; } // Timeout
      if (res == -1) { return -1;} // Other error
      res = send(socketfd, buffer_.data() + sent_bytes, num_bytes - sent_bytes, 0);
      if (res == -1) { return -1; }
      sent_bytes += res;
    }
    return 0;
  }
  /**
   * @brief Pack any int type
   * @param num an int
   */
  template<typename T>
  void pack_ints_(T num) {
    std::size_t n = sizeof(T)/sizeof(std::byte);
    for (std::size_t i = 1; i <= n; ++i) {
      buffer_.push_back(static_cast<std::byte>(num >> (8*(n - i))));
    }
  }
  /**
   * @brief Pack any iterable
   * @param first Beginning of the iterable
   * @param last end of the iterable.
   */
  template<typename TInputIter>
  void pack_vectors_(TInputIter first, TInputIter last) {
    while (first != last) {
      this->operator()(*first++);
    }
  }
  std::vector<std::byte> buffer_;
  double timeout_;
};

/**
 * @brief A class that handles the incoming information
 * 
 * A ring buffer with extra steps
 * 
 * @author Mathias Hauan Arbo
 * @since Wed Jul 27 2022
 */
class Unpacker {
public:
  /**
   * @brief Constructor
   * @param capacity maximum capacity of the buffer.
   */
  Unpacker(std::size_t capacity = 2048)
  : buffer_(capacity, std::byte(0)), capacity_(capacity), read_idx_(0), timeout_(1) {};

  /**
   * @brief Receive the data of a specific command into the buffer
   * @param socketfd socket file descriptor
   * @param command Command to receive, anything else is skipped
   * @param max_skip maximum number of packages we can skip
   * @return number of skipped packages, -3 if the socket is likely closed, -2
   * if a timeout likely occurred, or -1 if an unknown error occurred. 
   */
  int receive_buffer(int socketfd, uint8_t command, std::size_t max_skip = 3) {
    std::size_t skipped_packages = 0;
    int res = 0;
    read_idx_ = 0;
    while (skipped_packages < max_skip) {
      // Receive all bytes in the header or timeout
      res = recv_all_(socketfd, 3);
      if (res < 0){ return res; }
      // Unpack header
      std::size_t pkg_size = read_uint16();
      uint8_t pkg_command = read_uint8();
      // Handle unexpected command, skip it
      if ((pkg_command != Command::RECV_ANY) && (pkg_command != command)) {
        if(res < 0) { return res; }
        skipped_packages++;
        read_idx_ = 0;
        continue;
      }
      // Got the right command
      res = recv_all_(socketfd, pkg_size - 3);
      if (res < 0) { return res; }
      read_idx_ = 0;
      break;
    }
    return skipped_packages;
  }

  /**
   * @brief Read an RTDEType by name
   * @throws std::invalid_argument if unexpected data type string is given.
   * @param type name of the type
   * @return value of desired type
   */
  RTDEType read_type(std::string type) {
    if (type == "BOOL") { return read_bool(); }
    if (type == "UINT8") { return read_uint8(); }
    if (type == "UINT32") { return read_uint32(); }
    if (type == "UINT64") { return read_uint64(); }
    if (type == "INT32") { return read_int32(); }
    if (type == "DOUBLE") { return read_double(); }
    if (type == "VECTOR3D") { return read_vector3d(); }
    if (type == "VECTOR6D") { return read_vector6d(); }
    if (type == "VECTOR6INT32") { return read_vector6int32(); }
    if (type == "VECTOR6UINT32") { return read_vector6uint32(); }
    throw std::invalid_argument(
      "Unpacker::read_type given a string not used in RTDEType. Known types are: "
      "BOOL, UINT8, UINT32, UINT64, INT32, DOUBLE, VECTOR3D, VECTOR6D, "
      "VECTOR6INT32, VECTOR6UINT32. Got: " + type + ".");
  }

  /**
   * @brief Read a bool from the buffer
   * @throws std::out_of_range if not enough data available
   * @return true or false
   */
  bool read_bool() { return read_uint8() != 0;  }

  /**
   * @brief Read an uint8 from the buffer
   * @throws std::out_of_range if not enough data available
   * @return the value
   */
  uint8_t read_uint8() { return read_int_<uint8_t>(); }

  /**
   * @brief Read an uint16 from the buffer
   * @throws std::out_of_range if not enough data available
   * @return the value
   */
  uint16_t read_uint16() { return read_int_<uint16_t>(); }

  /**
   * @brief Read an uint32 from the buffer
   * @throws std::out_of_range if not enough data available
   * @return the value
   */
  uint32_t read_uint32() { return read_int_<uint32_t>(); }

  /**
   * @brief Read an uint64 from the buffer
   * @throws std::out_of_range if not enough data available
   * @return the value
   */
  uint64_t read_uint64() { return read_int_<uint64_t>(); }

  /**
   * @brief Read an int32 from the buffer
   * @throws std::out_of_range if not enough data available
   * @return the value
   */
  int32_t read_int32() {return read_int_<int32_t>(); }


  /**
   * @brief Read double using type punning
   * 
   * The platform should have IEEE 754 precision on doubles
   * 
   * @return the value
   */
  double read_double() {
    check_bounds_(8);
    union punner {
      double dval;
      std::byte bval[8];
    } res{};
    for (std::size_t i = 1; i <= 8; ++i) {
      res.bval[8 - i] = buffer_[read_idx_];
      read_idx_++;
    }
    return res.dval;
  }
  

  /**
   * @brief Read a RTDE type VECTOR3D from the buffer
   * @throws std::out_of_range if not enough data available
   * @return array of 3 doubles
   */
  std::array<double,3> read_vector3d() { return read_vectord_<3>(); }

  /**
   * @brief Read a RTDE type VECTOR6D from the buffer
   * @throws std::out_of_range if not enough data available
   * @return array of 6 doubles
   */
  std::array<double,6> read_vector6d() { return read_vectord_<6>(); }

  /**
   * @brief Read a RTDE type VECTOR6INT32 from the buffer
   * @throws std::out_of_range if not enough data available
   * @return array of 6 int32_t
   */
  std::array<int32_t,6> read_vector6int32() { return read_vector6int_<int32_t>();  }

  /**
   * @brief Read a RTDE type VECTOR6UINT32 from the buffer
   * @throws std::out_of_range if not enough data available
   * @return array of 6 uint32_t
   */
  std::array<uint32_t,6> read_vector6uint32() { return read_vector6int_<uint32_t>(); }
  
  /**
   * @brief Read ASCII string, either of specific length or the rest of the buffer
   * @param length number of bytes in the string
   * @return string
   */
  std::string read_string(std::size_t length = 0) {
    if (length == 0) {
      length = get_available();
    } else {
      check_bounds_(length);
    }
    std::string res;
    std::transform(
      buffer_.begin() + read_idx_, buffer_.begin() + length + read_idx_, std::back_inserter(res),
      [](std::byte b){ return static_cast<char>(b);});
    return res;

  }
  /**
   * @brief Read the available bytes directly
   * @return vector of bytes in the buffer
   */
  std::vector<std::byte> read_bytes() {
    std::vector<std::byte> res;
    std::copy(
      buffer_.begin() + read_idx_,
      buffer_.begin() + get_available() + read_idx_,
      res.begin());
    return res;
  }
  /**
   * @brief Set timeout for the send command
   * @param seconds
   */
  void set_timeout(double seconds) { timeout_ = seconds; }

  /**
   * @brief Set the buffer directly
   * @param buffer
   */
  void set_buffer(std::vector<std::byte> buffer) {
    // Just move/copy if necessary
    if (buffer.size() > buffer_.size()) {
      buffer_.resize(buffer.size());
      capacity_ = buffer.size();
    }
    std::copy(buffer.begin(), buffer.end(), buffer_.begin());
    read_idx_ = 0;
  }
  /**
   * @brief Get the maximum capacity of the unpacker
   * @return number of bytes
   */
  std::size_t get_capacity() { return capacity_; }
  /**
   * @brief Get the data available in the buffer
   * @return number of bytes
   */
  std::size_t get_available() { return available_data_ - read_idx_;}

private:
  /**
   * @brief Receive all
   * @param socketfd
   * @param num_bytes
   * @return -3 if likely socket is closed, -2 if timeout occurred, -1 if other error, 0 on success.
   */
  int recv_all_(int socketfd, std::size_t num_bytes) {
    fd_set file_desc_set;
    FD_ZERO(&file_desc_set);
    FD_SET(socketfd, &file_desc_set);
    std::size_t received_bytes = 0;
    int res;
    timeval timeout = sec2timeval(timeout_);
    while (received_bytes < num_bytes) {
      res = select(socketfd + 1, &file_desc_set, NULL, NULL, &timeout);
      if (res == 0) { return -2;} // Timeout
      if (res == -1) { return -1;} // Other error 
      res = recv(socketfd, buffer_.data() + received_bytes, num_bytes - received_bytes, 0);
      if (res == -1) { return -1; } // Other error
      if (res == 0) { return -3; } // Likely socket is closed
      received_bytes += res;
    }
    available_data_ = received_bytes;
    return 0;
  }
  /**
   * @brief Check if we have required bytes to read type
   * @param size number of bytes
   */
  void check_bounds_(std::size_t size) {
    if (read_idx_ + size > available_data_) {
      throw std::out_of_range(
        "Receive buffer has " + std::to_string(available_data_) +
        " bytes, but trying to read " + std::to_string(size) + 
        " bytes at from idx: " + std::to_string(read_idx_) + ".");
    }
  }
  /**
   * @brief Read any int type
   * @return int type
   */
  template<typename T>
  T read_int_() {
    std::size_t n = sizeof(T)/sizeof(std::byte);
    check_bounds_(n);
    T res = 0;
    for (std::size_t i = 1; i <= n; ++i) {
      res |= (static_cast<T>(buffer_[read_idx_]) << 8*(n - i));
      read_idx_++;
    }
    return res;
  }
  /**
   * @brief Read any vectord length
   * @return std::array<double,N>
   */
  template<std::size_t N>
  std::array<double, N> read_vectord_() {
    check_bounds_(8*N);
    std::array<double,N> res;
    for (std::size_t i = 0; i < N; ++i) {
      res[i] = read_double();
    }
    return res;
  }
  /**
   * @brief Read any vector6 int type
   * @return std::array<T,6>
   */
  template<typename T>
  std::array<T,6> read_vector6int_() {
    check_bounds_(sizeof(T)*6);
    std::array<T,6> res;
    for (std::size_t i = 0; i < 6; ++i) {
      res[i] = read_int_<T>();
    }
    return res;
  }

  std::vector<std::byte> buffer_;
  std::size_t read_idx_;
  std::size_t capacity_;
  std::size_t available_data_;
  double timeout_;
};

} // namespace: networking




/**
 * @brief Recipe object holding information
 *
 * The recipes are gathered from the recipe XML using the
 * "sm_rtde::parse_recipes" function. Each recipe gets a key associated with it
 * that is gathered from the XML. The recipe can be used for specifying input or
 * output in the robot controller. 
 *
 * If the recipe specifies input fields to the robot controller, the recipe is
 * uploaded using "control_package_setup_inputs", or if it specifies outputs it
 * is uploaded using "control_package_setup_outputs". For robot controller
 * outputs, the recipes are stored in the RTDE class. For robot controller
 * inputs, the user must store the recipe, set values, and command the robot
 * controller to set the field values to the desired value by using the RTDE
 * class' "send_data_package" function.
 *
 * Each field is stored as a DataObject. 
 *
 * @author Mathias Hauan Arbo
 * @since Tue Jul 26 2022
 */
class Recipe {
public:
  /**
   * @brief DataObject describing a field specified in an RTDE recipe.
   * @author Mathias Hauan Arbo
   * @since Tue Jul 26 2022
   */
  struct DataObject {
    DataObject(std::string type) : type(type) {};
    std::string type;  //<- The data type specified in the recipe
    RTDEType value;  //<- The current value of the data object
  };

  explicit Recipe(std::string key, std::vector<std::string> names, std::vector<std::string> types)
  : key_(key), names_in_order_(names), direction_(0) {
    for (int i = 0; i < names.size(); i++) {
      data_map_.emplace(
        names[i],
        types[i]
      );
    }
    id_ = -1;
    direction_ = 0;
  };

  /**
   * @brief Does the recipe represent input to the robot controller?
   * @return true or false
   */
  bool is_input() const { return direction_ == -1;}

  /**
   * @brief Does the recipe represent output from the robot controller?
   * @return true or false 
   */
  bool is_output() const { return direction_ == 1;}

  /**
   * @brief Is the recipe setup and ready to be used with the robot controller?
   * @return true or false
   */
  bool is_setup() const { return direction_ != 0;}

  /**
   * @brief Get the ID given to this recipe by the robot controller.
   * @return ID, or -1 if the recipe is not set up.
   */
  int get_id() const {return id_;}

  /**
   * @brief Set ID if not setup yet
   * @param id as received by the controller during setup
   */
  void set_id(int id) {
    if (id_ == -1) {
      id_ = id;
    }
  }
  
  /**
   * @brief Set direction if not setup yet
   * @param direction as defined by the RTDE class
   */
  void set_direction(int direction) {
    if (direction_ == 0) {
      direction_ = direction;
    }
  }

  /**
   * @brief Get the key used in the XML to represent this recipe
   * @return key
   */
  std::string get_key() const {return key_;}

  /**
   * @brief Get the data field names in order
   * @return vector of strings representing the names
   */
  std::vector<std::string> get_names() const {
    return names_in_order_;
  }

  /**
   * @brief Get the value stored in a field
   *
   * If you want to get the variant and figure out what it is later, use
   * RTDEType as the type in the template. 
   *
   * @param field_name in the recipe
   * @throw std::out_of_range if field_name is not found
   * @throw std::bad_variant_access if the requested type is not the type in the
   * field
   * @return value stored in a field
   */
  RTDEType get_field_value(std::string field_name) const {
    return data_map_.at(field_name).value;
  }

  /**
   * @brief Set the value stored in a field
   * @param field_name in the recipe
   * @param value to set
   * @throws std::out_of_range if field_name is not found
   */
  void set_field_value(std::string field_name, RTDEType value) {
    data_map_.at(field_name).value = value;
  }

  /**
   * @brief Get the data type of a field
   * @param field_name in the recipe
   * @throws std::out_of_range if field name is not found
   * @return string representing type as given in recipe
   */
  std::string get_field_type(std::string field_name) const {
    return data_map_.at(field_name).type;
  }

private:
  std::map<std::string, DataObject> data_map_;
  std::vector<std::string> names_in_order_;
  int id_;
  std::string key_;
  int direction_;
};

/**
 * @brief Parse recipes from an RTDE XML configuration file
 * @param filename path to the filename
 * @return map of shared pointers to the recipes
 */
std::map<std::string, std::shared_ptr<Recipe>> parse_recipes(std::filesystem::path filename) {
    // Proper file to test?
    if (std::filesystem::exists(filename) == false ||
        std::filesystem::is_regular_file(filename) == false) {
      return {};
    }
    std::map<std::string, std::shared_ptr<Recipe>> res;
    // Get the XML into rapidxml and find the rtde_config
    rapidxml::file<> xml_file(filename.c_str());
    rapidxml::xml_document<> doc;
    doc.parse<0>(xml_file.data());
    rapidxml::xml_node<>* rtde_config = doc.first_node("rtde_config");
    
    // Traverse the recipes
    rapidxml::xml_node<>* current_recipe_node = rtde_config->first_node("recipe");
    while (current_recipe_node) {
      // Gather data
      std::string key = current_recipe_node->first_attribute("key")->value();
      std::vector<std::string> names_in_order;
      std::vector<std::string> types_in_order;
      // Traverse the fields
      rapidxml::xml_node<>* current_field_node = current_recipe_node->first_node("field");
      while(current_field_node) {
        names_in_order.push_back(
          utils::trim_whitespace(
            current_field_node->first_attribute("name")->value()));
        types_in_order.push_back(
          utils::trim_whitespace(
            current_field_node->first_attribute("type")->value()));
        current_field_node = current_field_node->next_sibling();
      }
      // Prepare the next recipe node
      current_recipe_node = current_recipe_node->next_sibling();
      if (names_in_order.empty()) {
        // Found nothing? Ignore
        continue;
      }
      res.insert(
        std::make_pair(
          key, std::make_shared<Recipe>(key, names_in_order, types_in_order)
        ));
    }
    return res;
}


// RTDE functionality

/**
 * @brief Main class for RTDE interface
 * @author Mathias Hauan Arbo
 * @since Thu Jul 07 2022
 */
class RTDE {
public:
  /**
   * @brief Constructor
   * @param host IP of the UR controller
   * @param port default to 3004
   * @param verbose default to false
   * @param timeout send/receive port timeout, default 1 second.
   * @param output_frequency default 125, requires setting to protocol version 2.
   * @param buffer_size default to 4096 bytes.
   */
  explicit RTDE(std::string host, int port = 30004, bool verbose = false, double timeout = 1, double output_frequency = 125, std::size_t buffer_size = 4096)
  : host_name_(host), port_(port), verbose_(verbose), packer_(), unpacker_(buffer_size), output_frequency_(output_frequency) {
    socketfd_ = -1;
    skipped_package_count_ = 0;
    max_skipped_packages_ = 8;
    protocol_version_ = 2;
    set_timeout(timeout);
    conn_state_ = ConnectionState::DISCONNECTED;
  };
  
  ~RTDE() {
    disconnectRTDE();
  }

  enum MessageLevel {
    EXCEPTION_MESSAGE = 0,
    ERROR_MESSAGE = 1,
    WARNING_MESSAGE = 2,
    INFO_MESSAGE = 3
  };

  enum ConnectionState {
    DISCONNECTED = 0,
    CONNECTED = 1,
    STARTED = 2,
    PAUSED = 3
  };

  /**
   * @brief Connect to RTDE, set up socket
   * @return true on success, false otherwise
   */
  bool connectRTDE() {
    // Only start up if not already connected 
    if (conn_state_ == ConnectionState::CONNECTED) {
      return true;
    }
    skipped_package_count_ = 0;
    // Network options, taken from Universal Robots' python implementation
    int yes = 1;
    socketfd_ = socket(PF_INET, SOCK_STREAM, 0);
    setsockopt(socketfd_, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
    setsockopt(socketfd_, IPPROTO_TCP, TCP_NODELAY, &yes, sizeof(int));
    setsockopt(socketfd_, IPPROTO_TCP, TCP_QUICKACK, &yes, sizeof(int));
    
    
    // Setup network address stuff
    struct addrinfo hints, *res;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;
    const char * host_name__c = host_name_.empty() ? nullptr : host_name_.c_str();
    const char * service_c = std::to_string(port_).c_str();
    int status = getaddrinfo(host_name__c, service_c, &hints, &res);
    if ((status != 0)) {
      if (verbose_) {
        std::cerr << "connectRTDE: getaddrinfo error: " << gai_strerror(status) << std::endl;
      }
      freeaddrinfo(res);
      return false;
    }

    status = connect(socketfd_, res->ai_addr, res->ai_addrlen);
    if (status == -1) {
      if (verbose_) {
        // Debugging purposes
        std::cerr << "connectRTDE: Error during socket connect. errno: " << strerror(errno) << std::endl;
      }
      freeaddrinfo(res);
      return false;
    }
    conn_state_ = ConnectionState::CONNECTED;
    if (verbose_) {
      std::cout << "connectRTDE: connected.\n";
    }
    return true;
  }

  /**
   * @brief Disconnect from RTDE, close socket
   */
  void disconnectRTDE() {
    if (conn_state_ != ConnectionState::DISCONNECTED) {
      close(socketfd_);
      if (verbose_) {
        std::cout << "disconnectRTDE: disconnected.\n";
      }
    }
    conn_state_ = ConnectionState::DISCONNECTED;
  }

  /**
   * @brief Check if RTDE is in connected state
   * @return true if connected, false otherwise.
   */
  bool is_connected() {
    return conn_state_ != ConnectionState::DISCONNECTED;
  }

  /**
   * @brief Get the total number of skipped packages
   * @return total number of skipped packages
   */
  std::size_t get_total_skipped_package_count() {
    return skipped_package_count_;
  }

  /**
   * @brief Set number of packages that can be skipped during a receive call
   * @param nskips number of skips allowed, default 8
   */
  void set_max_skipped_packages(std::size_t nskips) {
    max_skipped_packages_ = nskips;
  }

  // General usage functions
  /**
   * @brief request the controller to work with "protocol_version"
   * @param protocol_version (likely either 1 or 2), default 2.
   * @return true if accepted, false otherwise
   */
  bool request_protocol_version(uint16_t protocol_version = 2) {
    packer_(protocol_version);
    packer_.pack_header(Command::RTDE_REQUEST_PROTOCOL_VERSION);
    if (!verified_send_()) {
      if (verbose_) {
        std::cerr << "request_protocol_version: could not send.\n";
      }
      return false;
    }
    if (!verified_receive_(Command::RTDE_REQUEST_PROTOCOL_VERSION)) {
      if (verbose_) {
        std::cerr << "request_protocol_version: could not receive.\n";
      }
      return false;
    }
    if (unpacker_.read_bool()) {
      protocol_version_ = protocol_version;
      return true;
    }
    return false;
  }

  /**
   * @brief Get URControl version
   * @return array of {Major, Minor, Bugfix, Build}
   */
  std::array<uint32_t, 4> get_urcontrol_version() {
    packer_.clear();
    packer_.pack_header(Command::RTDE_GET_URCONTROL_VERSION);
    if (!verified_send_()) {
      if (verbose_) {
        std::cerr << "get_urcontrol_version: could not send.\n";
      }
      return {};
    }
    if (!verified_receive_(Command::RTDE_GET_URCONTROL_VERSION)) {
      if (verbose_) {
        std::cerr << "get_urcontrol_version: could not receive.\n";
      }
      return {};
    }
    return {unpacker_.read_uint32(), unpacker_.read_uint32(), unpacker_.read_uint32(), unpacker_.read_uint32()};
  }

  /**
   * @brief Send a message to UR controller
   * @param message text to display
   * @param source where the message comes from, default "C++ Client"
   * @param level MessageLevel, 0: Exception, 1: Error, 2: Warning, 3: Info (default)
   * @return true if message was sent, false if something went wrong in sending it.
   */
  bool send_text_message(std::string message,
    std::string source = "C++ Client",
    MessageLevel level = MessageLevel::INFO_MESSAGE) {
    if (conn_state_ != ConnectionState::CONNECTED) {
      std::cerr << "send_text_message: RTDE not connected.\n";
    }
    packer_(static_cast<uint8_t>(message.size()));
    packer_(message);
    packer_(static_cast<uint8_t>(source.size()));
    packer_(source);
    packer_(static_cast<uint8_t>(level));
    packer_.pack_header(Command::RTDE_TEXT_MESSAGE);
    return verified_send_();
  }

  /**
   * @brief Send an RTDE data package
   * 
   * Make sure that the recipe has been set up properly first using
   * "control_package_setup_inputs". 
   * @param recipe_key
   * @return 
   */
  bool send_data_package(const std::string& recipe_key) {
    if (key_map_.at(recipe_key)->is_input() == false) {
      return false;
    }
    return send_data_package(*key_map_.at(recipe_key));
  }
  /**
   * @brief Send an RTDE data package
   *
   * Make sure that the recipe has been set up properly first using
   * "control_package_setup_inputs".
   *
   * @param recipe a recipe defined for UR controller input that has been set
   * up.
   * @return true if sent, false otherwise.
   */
  bool send_data_package(const Recipe& recipe) {
    if (conn_state_ != ConnectionState::STARTED) {
      std::cerr << "send_data_package: Synchronization not started.\n";
      return false;
    }
    if (!recipe.is_input()) {
      if (verbose_) {
        std::cerr << "send_data_package: Recipe is not set as UR controller's input.\n";
      }
      return false;
    } else if (!recipe.is_setup()) {
      if (verbose_) {
        std::cerr << "send_data_package: Recipe is not setup.\n";
      }
      return false;
    }
    packer_(static_cast<uint8_t>(recipe.get_id()));
    std::vector<std::string> names = recipe.get_names();
    for (std::string name : names) {
      RTDEType temp = recipe.get_field_value(name);
      std::visit(packer_, temp);
    }
    packer_.pack_header(Command::RTDE_DATA_PACKAGE);
    return verified_send_();
  }

  /**
   * @brief Receive a data package from the UR Controller
   * @return true if successful, false otherwise
   */
  bool receive_data_package() {
    if (conn_state_ != ConnectionState::STARTED) {
      std::cerr << "receive_data_package: Synchronization not started.\n";
      return false;
    }
    if (!verified_receive_(Command::RTDE_DATA_PACKAGE)) {
      if (verbose_) {
        std::cerr << "receive_data_package: Error receiving response from controller.\n";
      }
      return false;
    }
    uint8_t id = unpacker_.read_uint8();
    auto search = id_map_.find(id);
    if (search == id_map_.end()) {
      std::cerr << "receive_data_package: Got an unknown recipe ID from the controller.\n";
      return false;
    }
    // Found one we know of, populate the data
    std::vector<std::string> names = search->second->get_names();
    for (std::string name : names) {
      std::string type_name = search->second->get_field_type(name);
      search->second->set_field_value(
        name, unpacker_.read_type(type_name));
    }
    return true;
  }

  /**
   * @brief Send a custom set up output recipe
   * @param recipe shared pointer the RTDE interface can adopt.
   * @return true if successful, false otherwise.
   */
  bool control_package_setup_outputs(std::shared_ptr<Recipe> recipe) {
    std::string recipe_key = recipe->get_key();
    if (conn_state_ != ConnectionState::CONNECTED) {
      if (verbose_) {
        std::cerr << "control_package_setup_outputs: Not connected or in data synchronization.\n";
        return false;
      }
    }
    if (protocol_version_ > 1) {
      packer_(output_frequency_);
    }
    std::vector<std::string> names = recipe->get_names();
    std::string request = comma_join_string_(names);
    packer_(request);
    packer_.pack_header(Command::RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS);
    if (!verified_send_()) {
      std::cerr << "control_package_setup_outputs: Failed at sending recipe.\n";
      return false;
    }
    if (!verified_receive_(Command::RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS)) {
      std::cerr << "control_package_setup_outputs: Failed at receiving recipe.\n";
      return false;
    }
    // Gather the results
    int id = 0;
    if (protocol_version_ > 1) {
      id = unpacker_.read_uint8();
    }
    std::string received_str = unpacker_.read_string();
    std::vector<std::string> types = comma_split_string_(received_str);
    
    // Verify that the controller accepted everything
    bool problem_found = false;
    for (std::size_t i = 0; i < names.size(); ++i) {
      std::string field_type = recipe->get_field_type(names[i]);
      if (types[i] == "NOT_FOUND") {
        problem_found = true;
        std::cerr << "control_package_setup_inputs: Controller could not find field named: " + names[i] 
                    + " during set up of outputs from recipe: " + recipe_key + ".\n";
        problem_found = true;
      }
      if (types[i] != field_type) {
        std::cerr << "control_package_setup_inputs: Controller expects field name: " + names[i]
                    + " to be of the type: " + types[i] + " but the recipe  with key"
                    + recipe_key + " has field type: "
                    + field_type + ".\n";
        problem_found = true;
      }
    }
    if (problem_found) { return false; }
    recipe->set_id(id);
    recipe->set_direction(1);
    key_map_.insert(
      std::make_pair(recipe_key, recipe));
    id_map_.insert(
      std::make_pair(id, recipe));
    return true;
  }

  /**
   * @brief Send a custom set up input recipe
   * @param recipe shared pointer the RTDE class can adopt.
   * @return true if successful, false otherwise.
   */
  bool control_package_setup_inputs(std::shared_ptr<Recipe> recipe) {
    std::string recipe_key = recipe->get_key();
    if (conn_state_ != ConnectionState::CONNECTED) {
      if (verbose_) {
        std::cerr << "control_package_setup_inputs: Not connected or in data synchronization.\n";
        return false;
      }
    }
    std::vector<std::string> names = recipe->get_names();
    std::string request = comma_join_string_(names);
    packer_(request);
    packer_.pack_header(Command::RTDE_CONTROL_PACKAGE_SETUP_INPUTS);
    if (!verified_send_()) {
      std::cerr << "control_package_setup_inputs: Failed at sending recipe.\n";
      return false;
    }
    if (!verified_receive_(Command::RTDE_CONTROL_PACKAGE_SETUP_INPUTS)) {
      std::cerr << "control_package_setup_inputs: Failed at receiving recipe.\n";
      return false;
    }
    // Gather the results
    int id = unpacker_.read_uint8();
    std::string received_str = unpacker_.read_string();
    std::vector<std::string> types = comma_split_string_(received_str);
    // Verify that the controller accepted everything
    bool problem_found = false;
    for (std::size_t i = 0; i < names.size(); ++i) {
      std::string field_type = recipe->get_field_type(names[i]);
      if (types[i] == "IN_USE") {
        problem_found = true;
        std::cerr << "control_package_setup_inputs: The controller reports that field name: " + names[i]
                    + " as defined in recipe: " + recipe_key +
                    + " is already in use as an input.\n";
      }
      if (types[i] == "NOT_FOUND") {
        problem_found = true;
        std::cerr << "control_package_setup_inputs: Controller could not find field named: " + names[i]
                    + " during set up of inputs from recipe: " + recipe_key + "\n";
      }
      if (types[i] != field_type) {
        std::cerr << "control_package_setup_inputs: Controller expects field name: " + names[i]
                    + " to be of the type: " + types[i] + " but the recipe with key "
                    + recipe_key + " has field type: "
                    + field_type + ".\n";
        problem_found = true;
      }
    }
    if (problem_found) { return false; }
    recipe->set_id(id);
    recipe->set_direction(-1);
    // Set up two maps pointing to the same recipe for two access methods.
    key_map_.insert(
      std::make_pair(recipe_key, recipe));
    return true;
  }

  /**
   * @brief Ask the UR Controller to start data synchronization.
   * @return True if UR controller accepted, false otherwise.
   */
  bool control_package_start() {
    packer_.clear();
    packer_.pack_header(Command::RTDE_CONTROL_PACKAGE_START);
    if (!verified_send_()) {
      if (verbose_) {
        std::cerr << "control_package_start: could not send.\n";
      }
      return false;
    }
    if (!verified_receive_(Command::RTDE_CONTROL_PACKAGE_START)) {
      if (verbose_) {
        std::cerr << "control_package_start: could not receive.\n";
      }
      return false;
    }
    bool res = unpacker_.read_bool();
    if (res) {
      conn_state_ = ConnectionState::STARTED;
    }
    return res;
  }
  /**
   * @brief Ask the UR Controller to stop data synchronization.
   * @return True if successful, false otherwise.
   */
  bool control_package_pause() {
    packer_.clear();
    packer_.pack_header(Command::RTDE_CONTROL_PACKAGE_PAUSE);
    if (!verified_send_()) {
      if (verbose_) {
        std::cerr << "control_package_pause: could not send.\n";
      }
      return false;
    }
    if (!verified_receive_(Command::RTDE_CONTROL_PACKAGE_PAUSE)) {
      if (verbose_) {
        std::cerr << "control_package_pause: could not receive.\n";
      }
    }
    bool res = unpacker_.read_bool();
    if (res) {
      conn_state_ = ConnectionState::PAUSED;
    }
    return res;
  }
  /**
   * @brief Set the timeout of both send and receive on the socket.
   * @param seconds timeout
   */
  void set_timeout(double seconds) {
    timeout_ = seconds;
    packer_.set_timeout(seconds);
    unpacker_.set_timeout(seconds);
  }
  /**
   * @brief Set whether the system is verbose on errors.
   * @param is_verbose
   */
  void set_verbose(bool is_verbose) {
    verbose_ = is_verbose;
  }

  // RECIPE RELATED
  /**
   * @brief Get a value from an output recipe
   *
   * The template type must be in the RTDETypes: bool, uint8_t, uint32_t,
   * uint64_t, double, std::array<double,3>, std::array<double,6>,
   * std::array<int32_t,6>, std::array<uint32_t,6>
   *
   * @param recipe_key recipe that defines output from robot controller
   * @param field_name the specific field we want
   * @return the value in the field
   */
  template<typename T>
  T get_value(std::string recipe_key, std::string field_name) {
    static_assert(utils::IsContainedIn<T, RTDEType>::value, 
      "get_value must take an accepted RTDEType, see get_value documentation");
    return std::get<T>(key_map_.at(recipe_key)->get_field_value(field_name));
  }
  
  /**
   * @brief Set a value in an input recipe
   * @param recipe_key recipe that defines input to the robot controller
   * @param field_name the specific field we want
   * @param value the value to set to the field, must be RTDEType acceptable
   * @return true if recipe_key is an input recipe, false otherwise
   */
  bool set_value(std::string recipe_key, std::string field_name, RTDEType value) {
    if (key_map_.at(recipe_key)->is_output()) { return false; }
    key_map_.at(recipe_key)->set_field_value(field_name, value);
    return true;
  }

  /**
   * @brief Parse an RTDE XML file, and call appropriate RTDE setup functions
   * @param config_filename path to RTDE XML configuration file
   * @param output_recipes names of the output recipes
   * @param input_recipes names of the input recipes
   * @return true on success, false otherwise
   */
  bool parse_and_setup_recipes(std::filesystem::path config_filename, std::vector<std::string> output_recipes, std::vector<std::string> input_recipes) {
    std::map<std::string, std::shared_ptr<Recipe>> recipes = parse_recipes(config_filename);
    if (recipes.size() < output_recipes.size() + input_recipes.size()) {
      if (verbose_) {
        std::cerr << "parse_and_setup_recipes: The XML " << config_filename 
                  << " contains fewer recipes output_recipes.size() + input_recipes.size()\n";
      }
      return false;
    }
    for (std::string name : output_recipes) {
      auto search = recipes.find(name);
      if (search == recipes.end()) {
        if (verbose_) {
          std::cerr << "parse_and_setup_recipes: Output recipe " + name + " not found in " << config_filename << "\n";
        }
        return false;
      }
      if (!control_package_setup_outputs(recipes.at(name))) {
        if (verbose_) {
          std::cerr << "parse_and_setup_recipes: Could not setup output recipe " + name + "\n";
        }
        return false;
      }
    }
    for (std::string name : input_recipes) {
      auto search = recipes.find(name);
      if (search == recipes.end()) {
        if (verbose_) {
          std::cerr << "parse_and_setup_recipes: Input recipe " + name + " not found in " << config_filename << "\n";
        }
        return false;
      }
      if (!control_package_setup_inputs(recipes.at(name))) {
        if (verbose_) {
          std::cerr << "parse_and_setup_recipes: Could not setup input recipe " + name + "\n";
        }
        return false;
      }
    }
    return true;
  }

  /**
   * @brief Get the recipe keys to the input recipes
   * @return vector of strings
   */
  std::vector<std::string> get_input_recipe_keys() {
    std::vector<std::string> keys;
    for (auto &[key, value] : key_map_)  {
      if (value->is_input()) { keys.push_back(key); }
    }
    return keys;
  }

  /**
   * @brief Get the recipe keys to the output recipes 
   * @return 
   */
  std::vector<std::string> get_output_recipe_keys() {
    std::vector<std::string> keys;
    for (auto &[key, value] : key_map_) {
      if (value->is_output()) { keys.push_back(key); }
    }
    return keys;
  }
  /**
   * @brief Get the value fields in a recipe
   * @param recipe_key
   * @return vector of field names
   */
  std::vector<std::string> get_recipe_fields(std::string recipe_key) {
    if (key_map_.find(recipe_key) == key_map_.end()) {
      std::cerr << "get_recipe_fields: Unknown recipe key " + recipe_key + "\n";
      return {};
    }
    return key_map_.at(recipe_key)->get_names();
  }
  /**
   * @brief Get the type specified in the recipe for a specific value field
   * @param recipe_key
   * @param field_name
   * @return type as specified in the XML configuration file.
   */
  std::string get_recipe_field_type(std::string recipe_key, std::string field_name) {
    if (key_map_.find(recipe_key) == key_map_.end()) {
      std::cerr << "get_recipe_field_type: Unknown recipe key " + recipe_key + "\n";
      return {};
    }
    return key_map_.at(recipe_key)->get_field_type(field_name);
  }

private:
  bool verified_send_() {
    if (conn_state_ == ConnectionState::DISCONNECTED) {
      return false;
    }
    int res = packer_.send_buffer(socketfd_);
    if (res == 0) {
      return true;
    }
    if (res == -1) {
      std::cerr << "verified_send: Unknown error occurred during send, errno: " + std::string(strerror(errno)) + "\n";
      trigger_disconnected_();
    }
    if (res == -2) {
      std::cerr << "verified_send: Send timeout after " << std::to_string(timeout_) << " seconds.\n";
    }
    return false;
  }
  bool verified_receive_(Command command) {
    if (conn_state_ == ConnectionState::DISCONNECTED) {
      return false;
    }
    int res = unpacker_.receive_buffer(socketfd_, command, max_skipped_packages_);
    if (res >= 0) {
      skipped_package_count_ += res;
      return true;
    }
    if (res == -1) {
      std::cerr << "verified_receive: Unknown error occurred during receive, errno: " + std::string(strerror(errno)) + "\n";
      trigger_disconnected_();
    }
    if (res == -2) {
      std::cerr << "verified_receive: Receive timeout after " << std::to_string(timeout_) << " seconds.\n";
    }
    if (res == -3) {
      std::cerr << "verified_receive: Received 0 bytes, likely reason: Socket closed by controller.\n";
      trigger_disconnected_();
    }
    return false;
  }
  void trigger_disconnected_() {
    close(socketfd_);
    conn_state_ = ConnectionState::DISCONNECTED;
    std::cout << "trigger_closed: RTDE has triggered a disconnect!\n";
  }
  std::string comma_join_string_(std::vector<std::string> elems) {
    std::string comma_separated = elems[0];
    for (auto curr = elems.begin()+1, end = elems.end(); curr != end; ++curr) {
      comma_separated += "," + *curr;
    }
    return comma_separated;
  }
  std::vector<std::string> comma_split_string_(std::string received_string) {
    std::vector<std::string> res;
    std::stringstream ss(received_string);
    std::string item;
    while (std::getline(ss,item, ',')) {
      res.push_back(
        utils::trim_whitespace(item));
    }
    return res;
  }
  // Socket related
  std::string host_name_;
  int socketfd_;
  int port_;

  // Communication related
  double timeout_; // seconds
  std::size_t skipped_package_count_;
  std::size_t max_skipped_packages_;
  
  // Packing/Unpacking related
  std::vector<std::byte> buffer_;
  networking::Packer packer_;
  networking::Unpacker unpacker_;

  // General
  int protocol_version_;
  bool verbose_;
  ConnectionState conn_state_;
  std::map<int, std::shared_ptr<Recipe>> id_map_;
  std::map<std::string, std::shared_ptr<Recipe>> key_map_;
  double output_frequency_;
};

}  // sm_rtde
#endif //  UR_RTDE_HPP